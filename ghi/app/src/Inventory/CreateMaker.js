import React, { useState, useEffect } from "react";

function CreateMaker() {
    const [manufacturers, setMakers] = useState([]);
    const [formData, setFormData] = useState({
        name: "",
    })
    const getData = async () => {
        const response = await fetch('http://localhost:8100/api/manufacturers/');
        if (response.ok) {
            const data = await response.json();
            setMakers(data.manufacturers)
        }
    }
    useEffect(() => {
        getData();
    }, []);

    const handleSubmit = async (e) => {
        e.preventDefault();

        const manufacturerUrl = 'http://localhost:8100/api/manufacturers/';
            
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json'
            },
        };

        const response = await fetch(manufacturerUrl, fetchConfig);

        if (response.ok) {
            setFormData({
                name: '',
            })
            alert("Manufacturer created successfully!");
        }
    }
    const handleChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value,
        });
    }
    return (
        <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Manufacturer Form</h1>
          <form onSubmit={handleSubmit}>
            <div className="form-floating mb-3">
              <input onChange={handleChange} value={formData.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="name">Name</label>
            </div>
            <button type="submit" className="btn btn-primary">
              Add Manufacturer
            </button>
          </form>
        </div>
      </div>
    </div>
    )
}

export default CreateMaker