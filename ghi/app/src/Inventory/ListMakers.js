import React, { useState, useEffect } from 'react';

function MakersList() {
    const [manufacturers, setMakes] = useState([]);
    // when creating a state the first parameter must match the name of something, maybe the object name? Cannot be custom variable like "makes"

    const getData = async () => {
        const response = await fetch('http://localhost:8100/api/manufacturers/');

        if (response.ok) {
            const data = await response.json();
            setMakes(data.manufacturers)    

        }
    }

    useEffect(() => {
        getData();
    }, []);

    return (
<>
        <div className="container">
            <h2>Manufacturer List</h2>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Name</th>
            </tr>
          </thead>
          <tbody>
            {manufacturers.map( make => {
              return (
                <tr key={make.id}>
                  <td>{ make.name }</td>
                </tr>
              );
            })}
          </tbody>
        </table>
        </div>
        </>
    );
  }

export default MakersList;