import React, { useState, useEffect } from "react";

function CreateSale() {
  const [price, setPrice] = useState("");
  const [salesperson, setSalesperson] = useState([]);
  const [selectedSalesperson, setSelectedSalesperson] = useState("");
  const [customer, setCustomer] = useState([]);
  const [selectedCustomer, setSelectedCustomer] = useState("");
  const [automobile, setAutomobile] = useState([]);
  const [selectedAutomobile, setSelectedAutomobile] = useState("");

  const fetchData = async () => {
    const salesPersonUrl = "http://localhost:8090/api/salespeople/";
    const response = await fetch(salesPersonUrl);
    if (response.ok) {
      const salesPersonContent = await response.json();
      setSalesperson(salesPersonContent.Salespeople);
    }


    const customerUrl = "http://localhost:8090/api/customers/";
    const customerInfoResponse = await fetch(customerUrl);
    if (customerInfoResponse.ok) {
      const customerContent = await customerInfoResponse.json();
      setCustomer(customerContent.Customers);
    }

    const automobileUrl = "http://localhost:8100/api/automobiles/";
    const automobileInfoResponse = await fetch(automobileUrl);
    if (automobileInfoResponse.ok) {
      const automobileContent = await automobileInfoResponse.json();
      setAutomobile(automobileContent.autos);
    }
  };

  const submitHandler = async (event) => {
    event.preventDefault();

    const newSale = {
      price: price,
      salesperson: selectedSalesperson,
      customer: selectedCustomer,
      vin: selectedAutomobile,
    };

    const url = "http://localhost:8090/api/sales/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(newSale),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      alert("Form Submitted Successfully");

      const automobileDetailUrl = `http://localhost:8100/api/automobiles/${selectedAutomobile}/`;
      const automobileResponse = await fetch(automobileDetailUrl);
      if (automobileResponse.ok) {
        const automobileContent = await automobileResponse.json();
        const updatedAutomobile = {
          automobileContent,
          sold: true,
        };
        const putConfig = {
          method: "PUT",
          body: JSON.stringify(updatedAutomobile),
          headers: {
            "Content-Type": "application/json",
          },
        };
        const putResponse = await fetch(automobileDetailUrl, putConfig);
        if (putResponse.ok) {
          console.log("Automobile updated");
        } else {
          console.log("Error updating automobile");
        }
      } else {
        console.log("Error fetching automobile");
      }
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Salesperson History</h1>
            <form id="create-location-form" onSubmit={submitHandler}>
              <label htmlFor="price" className="mb-1">
                Price
              </label>
              <div className="form-floating mb-3">
                <input
                  className="form-control"
                  placeholder="Price"
                  type="text"
                  value={price}
                  onChange={(event) => setPrice(event.target.value)}
                />
              </div>
              <label htmlFor="salesPerson" className="mb-1">
                Salesperson
              </label>
              <div className="form-floating mb-3">
                <select
                  className="form-select"
                  value={selectedSalesperson}
                  onChange={(event) => setSelectedSalesperson(event.target.value)}
                >
                  <option value="">Select Salesperson</option>
                  {salesperson.map((person, index) => (
                    <option key={index} value={person.id}>
                      {person.first_name + " " + person.last_name}
                    </option>
                  ))}
                </select>
              </div>

              <label htmlFor="customer" className="mb-1">
                Customer
              </label>
              <div className="form-floating mb-3">
                <select
                  className="form-select"
                  value={selectedCustomer}
                  onChange={(event) => setSelectedCustomer(event.target.value)}
                >
                  <option value="">Select Customer</option>
                  {customer.map((person, index) => (
                    <option key={index} value={person.phone_number}>
                      {person.first_name + " " + person.last_name}
                    </option>
                  ))}
                </select>
              </div>

              <label htmlFor="vin" className="mb-1">
                VIN
              </label>
              <div className="form-floating mb-3">
                <select
                  className="form-select"
                  value={selectedAutomobile}
                  onChange={(event) => setSelectedAutomobile(event.target.value)}
                >
                  <option value="">Select VIN</option>
                  {automobile
                    .filter((car) => car.sold === false)
                    .map((car, index) => (
                      <option key={index} value={car.vin}>
                        {car.vin}
                      </option>
                    ))}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default CreateSale;
