import { useEffect, useState } from 'react';
import {Link} from 'react-router-dom'

function ListSale() {
  const [sale, setSale] = useState([]);

  const fetchData = async () => {
      const url = 'http://localhost:8090/api/sales/';

      const response = await fetch(url);
      if (response.ok){
        const content = await response.json();
        setSale(content.sales);
      }
    }
    useEffect(() => {
      fetchData();
    }, []);
  return(
    <>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Price</th>
            <th>Salespserson</th>
            <th>Customer</th>
            <th>VIN</th>
          </tr>
        </thead>
        <tbody>
          {sale?.map((data, index) => (
            <tr key={index}>
              <td>{"$" + data.price}</td>
              <td>{data.salesperson.first_name + " " + data.salesperson.last_name}</td>
              <td>{data.customer.first_name + " " + data.customer.last_name}</td>
              <td>{data.vin.vin}</td>
            </tr>
          ))}
        </tbody>
      </table>
      <Link to="/sales/create" className='btn btn-primary'>Create Sale</Link>
    </>
  )
}

export default ListSale;
