import React, { useState } from "react";

function CreateCustomer() {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [address, setAddress] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");

  const submitHandler = async (event) => {
    event.preventDefault();

    const newCustomer = {
      first_name: firstName,
      last_name: lastName,
      address:address,
      phone_number:phoneNumber
    };

    const url = "http://localhost:8090/api/customers/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(newCustomer),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      alert("Form Submitted Successfully");
    }
  };

  return (
    <>
      <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Add a Customer</h1>
              <form id="create-location-form" onSubmit={submitHandler}>
              <label htmlFor="firstName" className="mb-1">First Name</label>
                <div className="form-floating mb-3">
                  <input
                    className="form-control"
                    placeholder="First Name..."
                    type="text"
                    value={firstName}
                    onChange={(event) => setFirstName(event.target.value)}
                  />
                </div>
                <label htmlFor="lastName" className="mb-1">Last Name</label>
                <div className="form-floating mb-3">
                  <input
                    className="form-control"
                    placeholder="Last Name..."
                    type="text"
                    value={lastName}
                    onChange={(event) => setLastName(event.target.value)}
                  />
                </div>
                <label htmlFor="lastName" className="mb-1">Address</label>
                <div className="form-floating mb-3">
                  <input
                    className="form-control"
                    placeholder="Address..."
                    type="text"
                    value={address}
                    onChange={(event) => setAddress(event.target.value)}
                  />
                </div>
                <label htmlFor="lastName" className="mb-1">Phone Number</label>
                <div className="form-floating mb-3">
                  <input
                    className="form-control"
                    placeholder="Phone Number..."
                    type="text"
                    value={phoneNumber}
                    onChange={(event) => setPhoneNumber(event.target.value)}
                  />
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default CreateCustomer;
