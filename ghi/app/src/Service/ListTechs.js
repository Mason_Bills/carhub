import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

function Technicians() {
  const [technicians, setTechnicians] = useState([]);
  
  useEffect(() => {
    async function loadTechnicians() {
      const response = await fetch('http://localhost:8080/api/technicians/');
      if (response.ok) {
        const data = await response.json();
        setTechnicians(data.technicians);
      } else {
        console.error(response);
      }
    }
    loadTechnicians();
  }, []); 



    return (
        <>
        <div className="container">
        <table className="table table-striped">
          <thead>
            <tr>
              <th>First Name</th>
              <th>Last Name</th>
              <th>Employee ID</th>
            </tr>
          </thead>
          <tbody>
            {technicians.map(technician => {
              return (
                <tr key={technician.employee_id}>
                  <td>{ technician.first_name }</td>
                  <td>{ technician.last_name }</td>
                  <td>{ technician.employee_id }</td>
                </tr>
              );
            })}
          </tbody>
        </table>
        <div>
              <Link to="/technicians/create" className="btn btn-primary btn-lg px-4 gap-3">
          Add a Technician
        </Link>
      </div>
        </div>
        </>
    );
  }


export default Technicians;
