import React, { useEffect, useState } from 'react';



function CreateAppointment() {
  const [technicians, setTechnicians] = useState([]);
  const [formData, setFormData] = useState({
    vin: "",
    customer: "",
    date: "",
    time: "",
    reason: "",
    technician: "",
  });

  const getData = async () => {
    const data = 'http://localhost:8080/api/technicians/';
    const response = await fetch(data);
    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
    }
  }
console.log(technicians);
  useEffect(() => {
    getData();
  }, []);
  const handleSubmit = async (e) => {
    e.preventDefault();

    const appointment = {
      vin: formData.vin,
      customer: formData.customer,
      date: formData.date,
      time: formData.time,
      reason: formData.reason,
      technician: formData.technician,
    };

    try {
      const response = await fetch('http://localhost:8080/api/appointments/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(appointment),
      });
    if (!response.ok) {
      throw new Error('Response not ok');
    }

    const newAppointment = await response.json();
    console.log(newAppointment);
    alert("Appointment created successfully!");
    
    setFormData({
      vin: '',
      customer: '',
      date: '',
      time: '',
      reason: '',
      technician: '',
    });
    } catch (error) {
      alert("There was an error creating the appointment");
    }
  };


  const handleFormChange = (e) => {
    const inputName = e.target.name;
    const value = e.target.value;

    setFormData({
      ...formData,
      [inputName]: value,
    });
}

  return (
    <>
    <div className="container">
    <div className='shadow-lg p-3 mb-5 mt-5 bg-white rounded w-50 mx-auto'>
        <h1 className="my-4">Create New Appointment</h1>
        <form onSubmit={handleSubmit}>
            <div className="mb-3">
            <label htmlFor="vin" className="form-label">VIN</label>
                <input type="text" className="form-control" id="vin" name="vin" value={formData.vin} onChange={handleFormChange} />
            </div>
            <div className="mb-3">
                <label htmlFor="customer" className="form-label">Customer</label>
                <input type="text" className="form-control" id="customer" name="customer" value={formData.customer} onChange={handleFormChange} />
            </div>
            <div className="mb-3">
                <label htmlFor="date" className="form-label">Date</label>
                <input type="date" className="form-control" id="date" name="date" value={formData.date} onChange={handleFormChange} />
            </div>
            <div className="mb-3">
                <label htmlFor="time" className="form-label">Time</label>
                <input type="time" className="form-control" id="time" name="time" value={formData.time} onChange={handleFormChange} />
            </div>
            <div className="mb-3">
                <label htmlFor="reason" className="form-label">Reason</label>
                <input type="text" className="form-control" id="reason" name="reason" value={formData.reason} onChange={handleFormChange} />
            </div>
            <div className="mb-3">
                <select onChange={handleFormChange} value={formData.technician} required name="technician" id="technician" className="form-select">
                    <option value="">Choose a technician</option>
                    {technicians.map(technician => {
                        return (
                            <option key={technician.employee_id} value={technician.employee_id}>{technician.first_name}</option>
                        )
                    })}
                </select>
            </div>
            <button type="submit" className="btn btn-primary">Submit</button>
        </form>
        </div>
    </div>
</>
);
}


export default CreateAppointment;