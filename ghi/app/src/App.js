import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './Sale/MainPage';
import Nav from './Nav';
import CreateTechnician from './Service/CreateTech';
import CreateAppointment from './Service/CreateAppointment';
import ListTechinicians from './Service/ListTechs';
import ListAppointments from './Service/ListAppoint';
import ServiceHistory from './Service/ServiceHistory';
import ListCars from './Inventory/ListAuto';
import ModelList from './Inventory/ListModels';
import MakersList from './Inventory/ListMakers';
import CreateMaker from './Inventory/CreateMaker';
import CreateModel from './Inventory/CreateModel';
import CreateAuto from './Inventory/CreateAuto';
import ListSalesperson from "./Sale/ListSalesperson";
import ListCustomer from "./Sale/ListCustomer";
import CreateCustomer from "./Sale/CreateCustomer";
import CreateSalesperson from "./Sale/CreateSalesperson";
import CreateSale from "./Sale/CreateSale";
import ListSale from "./Sale/ListSale";
import HistorySalesperson from "./Sale/HistorySalesperson";
function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="" element={<MainPage />} />
          <Route path="technicians">
            <Route path="" element={<ListTechinicians />} />
            <Route path="create" element={<CreateTechnician/>} />
          </Route>
          <Route path="appointments">
            <Route path="" element={<ListAppointments/>} />
            <Route path="create" element={<CreateAppointment/>} />
          </Route>
          <Route path='inventory'>
            <Route path="cars" element={<ListCars/>} />
            <Route path="models" element={<ModelList/>} />
            <Route path="manufacturers" element={<MakersList/>} />
            <Route path="create/manufacturer" element={<CreateMaker/>} />
            <Route path="create/model" element={<CreateModel/>} />
            <Route path="create/auto" element={<CreateAuto/>} />
          </Route>
          <Route path="servicehistory" element={<ServiceHistory/>} />
          <Route path="salespeople">
            <Route path="" element={<ListSalesperson />} />
            <Route path="create" element={<CreateSalesperson />} />
          </Route>
          <Route path="customers">
            <Route path="" element={<ListCustomer />} />
            <Route path="create" element={<CreateCustomer />} />
          </Route>
          <Route path="sales">
            <Route path="" element={<ListSale />} />
            <Route path="create" element={<CreateSale />} />
            <Route path="history" element={<HistorySalesperson />} />
            </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
