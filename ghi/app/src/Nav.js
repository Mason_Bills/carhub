import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container">
        <div className='d-flex flex-wrap justify-content-center'>
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <NavLink className="navbar-brand" to="/technicians">Technicians</NavLink>
        <NavLink className="navbar-brand" to="/technicians/create">Add a Technician</NavLink>
        <NavLink className="navbar-brand" to="/appointments">Appointments</NavLink>
        <NavLink className="navbar-brand" to="/appointments/create">Make an Appointment</NavLink>
        <NavLink className="navbar-brand" to="/servicehistory">Service History</NavLink>
      </div>
      <div>
                <NavLink className="navbar-brand" to="/inventory/cars">Cars</NavLink>
        <NavLink className="navbar-brand" to="/inventory/models">Models</NavLink>
        <NavLink className="navbar-brand" to="/inventory/manufacturers">Manufacturers</NavLink>
        <NavLink className="navbar-brand" to="/inventory/create/manufacturer">Create Manufacturer</NavLink>
        <NavLink className="navbar-brand" to="/inventory/create/model">Create Model</NavLink>
        <NavLink className="navbar-brand" to="/inventory/create/auto">Create Automobile</NavLink>
      </div>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className='nav-item d-flex flex-wrap justify-content-center'>
              <NavLink className="navbar-brand" aria-current="page" to="/salespeople">List Salespeople</NavLink>
              <NavLink className="navbar-brand" aria-current="page" to="/salespeople/create">Create Salesperson</NavLink>
              <NavLink className="navbar-brand" aria-current="page" to="/customers">List Customers</NavLink>
              <NavLink className="navbar-brand" aria-current="page" to="/customers/create">Create Customer</NavLink>
              <NavLink className="navbar-brand" aria-current="page" to="/sales">List Sale</NavLink>
              <NavLink className="navbar-brand" aria-current="page" to="/sales/create">Create Sale</NavLink>
              <NavLink className="navbar-brand" aria-current="page" to="/sales/history">Salesperson History</NavLink>
            </li>
          </ul>
          </div>
        </div>
    </nav>
  )
}

export default Nav;
