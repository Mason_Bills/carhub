from common.json import ModelEncoder
from .models import AutomobileVO, Technician, Appointment


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin"]
   

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id"]


class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "vin",
        "customer",
        "date",
        "time",
        "reason",
        "vip",
        "finished",
        "cancelled",
        "technician",
        "id",
    ]
    encoders = {
        "technician": TechnicianEncoder(),
    }


class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "vin",
        "customer",
        "date",
        "time",
        "reason",
        "vip",
        "finished",
        "cancelled",
        "id",
        "technician",
    ]

    encoders = {
        "technician": TechnicianEncoder(),
    }