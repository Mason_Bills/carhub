from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Salesperson, Customer, Sale, AutomobileVO
from .encoders import SalespersonEncoder, CustomerEncoder, SaleEncoder
import json


@require_http_methods(["GET", "POST"])
def salesperson_list(request):
    if request.method == "GET":
        salespeople = list(Salesperson.objects.all())
        return JsonResponse(
            {"Salespeople": salespeople}, encoder=SalespersonEncoder, safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            salesperson = Salesperson.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "Invalid salesperson"}, status=404)
        except KeyError:
            return JsonResponse({"message": "Invalid JSON"}, status=400)


@require_http_methods(["DELETE", "GET"])
def salesperson_detail(request, pk):

    if request.method == "GET":
        salesperson = Salesperson.objects.get(id=pk)
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        try:
            deleted_objects = Salesperson.objects.filter(id=pk)
            if deleted_objects.exists():
                deleted_objects.delete()
                return JsonResponse({"deleted": True})
            else:
                return JsonResponse({"deleted": False})
        except:
            return JsonResponse({"message": "Sale not deleted"})


@require_http_methods(["GET", "POST"])
def customer_list(request):
    if request.method == "GET":
        customers = list(Customer.objects.all())
        return JsonResponse(
            {"Customers": customers}, encoder=CustomerEncoder, safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Invalid customer ID"}, status=404)
        except KeyError:
            return JsonResponse({"message": "Invalid JSON payload"}, status=400)


@require_http_methods(["DELETE", "GET"])
def customer_detail(request, pk):

    if request.method == "GET":
        customer = Customer.objects.get(id=pk)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        try:
            deleted_objects = Customer.objects.filter(id=pk)
            if deleted_objects.exists():
                deleted_objects.delete()
                return JsonResponse({"deleted": True}, status=200)
            else:
                return JsonResponse({"deleted": False}, status=400)
        except:
            return JsonResponse({"message": "Sale not deleted"}, status=404)


@require_http_methods(["GET", "POST"])
def sale_list(request):
    if request.method == "GET":
        sales = list(Sale.objects.all())
        return JsonResponse({"sales": sales}, encoder=SaleEncoder, safe=False)
    else:
        content = json.loads(request.body)
        try:
            salesperson = Salesperson.objects.get(employee_id=content["salesperson"])
            content["salesperson"] = salesperson
            print(content["salesperson"])
            customer = Customer.objects.get(phone_number=content["customer"])
            content["customer"] = customer
            automobile = AutomobileVO.objects.get(vin=content["vin"])
            automobile.sold = True
            automobile.save()
            content["vin"] = automobile
            sale = Sale.objects.create(**content)
            return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe=False,
            )
        except AutomobileVO.DoesNotExist:
            return JsonResponse({"message": "Automobile not found"}, status=404)
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "Salesperson not found"}, status=404)
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Customer not found"}, status=404)
        except KeyError:
            return JsonResponse({"message": "Bad Request"}, status=400)


@require_http_methods(["DELETE", "GET"])
def sale_detail(request, pk):

    if request.method == "GET":
        sale = Sale.objects.get(id=pk)
        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        try:
            deleted_objects = Sale.objects.filter(id=pk)
            if deleted_objects.exists():
                deleted_objects.delete()
                return JsonResponse({"deleted": True})
            else:
                return JsonResponse({"deleted": False})
        except:
            return JsonResponse({"message": "Sale not deleted"})
